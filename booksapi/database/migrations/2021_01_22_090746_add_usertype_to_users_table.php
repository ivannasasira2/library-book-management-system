<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsertypeToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //superuser --usertype=1
            //forAdmin  --usertype=2 and librarian_id defined
            //forStudent --usertype=3 and student_id defined
            //middleware will handle this logic --------- instead of creating multiple tables
             $table->integer('usertype');
             $table->integer('librarian_id');
             $table->integer('student_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
