<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('book_id');
            $table->unsignedBigInteger('student_id');
            $table->unsignedBigInteger('librarian_id')->nullable();
            
            $table->date('auto_return_date');
            $table->date('return_date')->nullable();
 
            $table->foreign('student_id')
                    ->references('id')->on('students');
            $table->foreign('librarian_id')
                    ->references('id')->on('librarians');
            $table->foreign('book_id')
                    ->references('id')->on('books');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_requests');
    }
}
