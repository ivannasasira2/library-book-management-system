<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function __construct(){
         //default auth guard to api
         //auth()->setDefaultDriver('api');
    }

    public function is_auth_user(){
        try { 
            $user = auth('api')->UserOrFail();
        }catch(\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
              return response()->json(['error'=>$e->getMessage()]); 
        }

        //if user passes, now manage the user roles here
        //get user details
        $user = auth('api')->user();
        $usertype = $user->usertype;
        //$librarianId = $user->librarian_id;
        //$studentId = $user->student_id;
        //return response()->json(['user'=>$usertype]);
    } 

    public function is_admin_auth_user(){ 

        #MAIN AUTH----------------------------------
        if($resp = $this->is_auth_user()){ //this is a function for handling authentication
            return $resp;
        }

        #SUPER USER ROLE or ADMIN ROLE--------------------------
        //if user passes, now manage the user roles here
        //get user details
        $user = auth('api')->user();
        $usertype = $user->usertype;
        $librarianId = $user->librarian_id; 

        if($usertype==1 || ($usertype==2 && !empty($librarianId))){
             
        }else {
            $err_msg = "Permission Denied!";
            return response()->json(['error'=>$err_msg]);
        } 
    }


    public function is_student_auth_user(){ 

        #MAIN AUTH-------------------------------
        if($resp = $this->is_auth_user()){ //this is a function for handling authentication
            return $resp;
        }

        #SUPERUSER ROLE or STUDENT ROLE--------------------------
        //if user passes, now manage the user roles here
        //get user details
        $user = auth('api')->user();
        $usertype = $user->usertype;
        $studentId = $user->student_id; 
        
        if($usertype==1 || ($usertype==3 && !empty($studentId))){
             
        }else {
            $err_msg = "Permission Denied!";
            return response()->json(['error'=>$err_msg]);
        } 
    } 

}
