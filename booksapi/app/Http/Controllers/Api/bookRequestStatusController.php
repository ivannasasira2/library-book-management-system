<?php 
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Api\Controller;

use App\Student;
use App\Book;
use App\BookRequest;
use App\Librarian;
use Auth;

class bookRequestStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
      
    }

   
    public function index()
    {   
        /*
        if($resp = $this->is_admin_auth_user()){ //this is a function for handling authentication
            return $resp;
        }
        */

        //get all requests
    //    return BookRequest::all();
 
       
      return BookRequest::join('books', 'books.id', '=', 'book_requests.book_id')
                    ->leftjoin('students', 'students.id', '=', 'book_requests.student_id')
                    ->leftjoin('librarians', 'librarians.id', '=', 'book_requests.librarian_id')
                    ->select('book_requests.*','books.title as book_title','students.name as stud_name',
                    'students.regNo as stud_reg_no','students.studentNo as stud_no',
                    'librarians.name as lirarian_name')
                    ->get();
       
    }

    /**Shows all records */
    public function show_all()
    {
        //
        return ["Message"=>"Wallace"];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //adds a new record
        return BookRequest::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //queries for a particular id----------
    //    return BookRequest::find($id);
    return BookRequest::join('books', 'books.id', '=', 'book_requests.book_id')
                    ->leftjoin('students', 'students.id', '=', 'book_requests.student_id')
                    ->leftjoin('librarians', 'librarians.id', '=', 'book_requests.librarian_id')
                    ->select('book_requests.*','books.title as book_title','students.name as stud_name',
                    'students.regNo as stud_reg_no','students.studentNo as stud_no',
                    'librarians.name as lirarian_name')
                    ->where('book_requests.id', $id)
                    ->first(); 
    }

     
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //updates a particular record
        $student = BookRequest::find($id);
        $student->update($request->all());
        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete record
        return BookRequest::destroy($id);
    }
}
