<?php 

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 

use App\Http\Controllers\Api\Controller; //modified

use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

use Auth;
 
use App\User;

class usersLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 
  
     //Add a new user---------------------- laravel eloquent #Request  $request
    public function  login_api_user(){  
        $user_credentials = [  
            'email' => 'stevewallaceug@gmail.com',
            'password' => 'pass123'
        ];
        //in case its a request
        #$user_credentials = $request()->only(['email','password']);

        //$token = auth('api')->attempt($user_credentials);
        if(!$token = auth('api')->attempt($user_credentials)){
            $error_msg = "Incorrect Username/Password!";
            return response()->json(['slogin'=>'0','error'=>$error_msg]);
        }

       $userDetails = auth('api')->user(); 
       //, 'user'=>$userDetails

        return response()->json(['slogin'=>'1','token'=>$token,'user'=>$userDetails]);  
    }

    public function  get_newToken(){
        //getting a new token -------------
        //Tymon\JWTAuth\Exceptions\JWTException
        try { 
            $newToken = auth('api')->refresh();
        }catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
              return response()->json(['error'=>$e->getMessage()]); 
        } 
        return response()->json(['token'=>$newToken]); 
    }

    //return user-------------------------------
    public function  getlogged_in_api_user(){    
        if($resp = $this->is_auth_user()){ //this is a function for handling authentication
            return $resp;
        }
        
        $userDetails = auth('api')->user();  
        return $userDetails; 
    }
 


    public function  logout_api_user(){   
        auth('api')->logout(); 
        return "Logged out!";
    }



 
}
