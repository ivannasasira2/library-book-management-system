<?php 

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 

use App\Http\Controllers\Controller; //modified

use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

use Auth;
 
use App\User;

class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //this is to get all the records
        return User::all();
    }

    //Add a new user---------------------- laravel eloquent
    public function  create_super_user(Request $request){   
        $added_user = User::create([  #here you can do update, delete, 
            'name'=>'miracle',
            'email' => 'stevewallaceug@gmail.com',
            'password' => Hash::make('pass123'),
            'usertype' =>1
        ]);
        return $added_user;
    }
 
    public function  create_user(Request $request){   
        $user_details = $request()->only('name','email','password');
        $added_user = User::create([  #here you can do update, delete, 
            'name'=> $user_details->name,
            'email' => $user_details->email,
            'password' => Hash::make($user_details->password)
        ]); 
        return $added_user;
    }

  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //adds a new student
        return User::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //queries for a particular id----------
       return User::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //updates a particular record
        $user = User::find($id);
        $updated_user = $user->update($request->all());
        return $updated_user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete student
      //  return User::destroy($id);
    }
}
