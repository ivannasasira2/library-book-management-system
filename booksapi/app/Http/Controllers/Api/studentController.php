<?php 
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Api\Controller;

use App\Student;

use Auth;

class studentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
      
    }

   
    public function index()
    {   
        /*
        if($resp = $this->is_admin_auth_user()){ //this is a function for handling authentication
            return $resp;
        }
        */

        //get all students
        return Student::all();
    }

    //Add a new student---------------------- laravel eloquent
    public function  create_student(){  
        $added_student = Student::create([  #here you can do update, delete, 
            'regNo'=>'2019/HD05/25386',
            'studentNo' => '19007386',
            'name' => 'Kalungi John'
        ]);
        return $added_student;
    }

    /**Shows all records */
    public function show_all()
    {
        //
        return ["Message"=>"Wallace"];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //adds a new student
        return Student::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //queries for a particular id----------
       return Student::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //updates a particular record
        $student = Student::find($id);
        $student->update($request->all());
        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete student
        return Student::destroy($id);
    }
}
