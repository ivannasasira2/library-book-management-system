<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //fillable 
    protected $fillable = [
        'regNo',
        'studentNo',
        'name'
    ];

    /*
    #In case field is returned as a string---------------
    protected $casts = [
        'studentNo' => 'integer'
    ];
    */
}
