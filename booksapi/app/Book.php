<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //fillable 
    protected $fillable = [
        'title',
        'publication_date',
        'librarian_id'
    ];

    /*
    #In case field is returned as a string---------------
    protected $casts = [
        'studentNo' => 'integer'
    ];
    */
}
