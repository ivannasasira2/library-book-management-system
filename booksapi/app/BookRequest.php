<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookRequest extends Model
{
    //
    protected $fillable = [
        'book_id',
        'student_id',
        'auto_return_date',
        'is_issued',
        'librarian_id',
    ];
}
