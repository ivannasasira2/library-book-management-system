<?php
 

use Illuminate\Http\Request;

//Auth::routes();

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


#API will basically base on 5 Call Requests
// 1. Get all (GET) ---/api/posts
// 2. Get a Single record (GET) ---/api/posts/{id}
// 3. Create a post (POST) ---/api/posts
// 4. update a single (PUT/PATCH) ---/api/posts/{id} --vss even post can work but path changes//
// 5. delete a (DELETE) ---/api/posts/{id}


#create a resource posts in laravel
// create the database and migratons
// create a model
// create a service? Eloquent ORM
// create a controller to go get info fromthe database
// return that info

Route::get('/vssapi', function(){
    return ["Message"=>"V API"];
} 
);


#Books Module-----------------------------
Route::get('/books','Api\booksController@index')->name('data.books');
Route::post('/book/create','Api\booksController@store')->name('data.post.book');
Route::get('/books/{id}','Api\booksController@show')->name('data.find.book');
Route::post('/book/update/{id}','Api\booksController@update')->name('data.update.book');
Route::get('/book/del/{id}','Api\booksController@destroy_data')->name('data.destroy.book');
/*
Route::get('/api2/student','Api\studentController@index')->name('create.student.libinfoms');
Route::post('/api2/student','Api\studentController@store')->name('create.student.libinfoms');
Route::put('/api2/student','Api\studentController@update')->name('create.student.libinfoms');
Route::delete('/api2/student','Api\studentController@destroy')->name('create.student.libinfoms');
#but laravel comes with a resource controller that can bind all the routes...
*/

#Books Module --------------------------------


Route::get('/api2','Api\studentController@show_all')->name('show.libinfomis.students');

Route::resource('/student','Api\studentController');
/*
Route::get('/student','Api\studentController@index')->name('create.student.libinfoms');
Route::post('/student','Api\studentController@store')->name('create.student.libinfoms');
Route::put('/student','Api\studentController@update')->name('create.student.libinfoms');
Route::delete('/student','Api\studentController@destroy')->name('create.student.libinfoms');
#but laravel comes with a resource controller that can bind all the routes...
*/

Route::resource('/book/request','Api\bookRequestController');
Route::get('/book/to-request','Api\bookRequestController@get_books_toRequest');
Route::post('/book/request/issue/{id}','Api\bookRequestController@update_bk');

#we can also prefix it
/*
Route::prefix('v1')->group(function(){
    Route::resource('/student','Api\studentController');
});
*/

//Route::post('login', [ 'as' => 'login', 'uses' => 'LoginController@do'])
Route::get('/login', function(){
         return "Log in";

})->name('login.libinfomis');


Route::get('/api2/addstudent', function(){
         

})->name('add.libinfomis.addstudent');


//api token get by post libinfoms
Route::post('/libinfoms-login-api-user', 'Api\usersLoginController@login_api_user')->name('post.libinfomis.login.user');

//
Route::middleware('vss_cors')->get('/login-api-user', 'Api\usersLoginController@login_api_user')->name('libinfomis.login.user');
Route::get('example', array('middleware' => 'cors', 'uses' => 'ExampleController@dummy'));


 
Route::get('/get-login-api-user', 'Api\usersLoginController@getlogged_in_api_user')->name('libinfomis.login.get_logged_in_user');
Route::get('/refreshtoken', 'Api\usersLoginController@get_newToken')->name('libinfomis.login.newtoken');


Route::get('/create-user', 'Api\usersController@create_user')->name('create.libinfomis.user');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
