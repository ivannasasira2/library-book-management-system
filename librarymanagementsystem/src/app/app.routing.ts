import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { DfComponent } from './pages/books/df.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

export const AppRoutes: Routes = [
 
  {
    path: 'mybook',
    component: AdminLayoutComponent,// DfComponent,
    children: [
      {
        path : 'ch',
        component: DashboardComponent
      }
    ]
  },
/* 
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  
   {
    path: '',
    component: AdminLayoutComponent, //DfComponent
    children: [
        {
      path: '',
      loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
  }]},
  {
    path: '**',
    redirectTo: 'dashboard'
  },
  */
]
