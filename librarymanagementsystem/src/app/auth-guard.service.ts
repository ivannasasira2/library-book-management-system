import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
//implemennt CanActivate
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(private localStorageService: LocalStorageService, 
    private route: Router) { }

  //return if user is logged in ---true or invalid
  canActivate() : boolean {

    if( !this.localStorageService.retrieve('user')) {
      //redirect to login
      this.route.navigate(['user/login']);
      return false;
    }

    //check the roles from here
    let userType = this.localStorageService.retrieve('user').user.usertype;

    //alert('This is s'+userType);

    if(!userType){
      //redirect to login
      this.route.navigate(['user/login']);
      return false;
    }else if(userType==1 || userType==2){ //Admin Dashboard --check librarian id
      return true;
      //this.route.navigate(['admin/dashboard']);
      //return false;
    }else if(userType==3){ //Student Portal ---student id
      return true;
      //this.route.navigate(['student/book-requests']); //kinda interchanged
     // return false;
    }

    //redirect to login page or authorized page...
    alert('Not logged in');
    this.route.navigate(['user/login']);
    return false;
    
  }

  //return if user is logged in ---true or invalid
  canActivateChild() : boolean {

    if(!this.localStorageService.retrieve('user')
     && (this.localStorageService.retrieve('user').slogin!=1)) {
      //redirect to login 
      return false;
    }

    //redirect to login page or authorized page...
    alert('Child log-in not allowed'); 
    return false;
}

}
