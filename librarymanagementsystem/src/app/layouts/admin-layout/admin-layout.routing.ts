import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { UserComponent } from '../../pages/user/user.component';
import { TableComponent } from '../../pages/table/table.component';
import { TypographyComponent } from '../../pages/typography/typography.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { NotificationsComponent } from '../../pages/notifications/notifications.component';
import { UpgradeComponent } from '../../pages/upgrade/upgrade.component';
import { BookComponent } from 'app/pages/books/book.component';
import { BookrequestsComponent } from 'app/pages/books/bookrequests.component';
import { StudentComponent } from 'app/pages/students/student.component';
import { BookrequestshistoryComponent } from 'app/pages/books/bookrequestshistory.component';


export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user',           component: UserComponent },
    { path: 'table',          component: TableComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'upgrade',        component: UpgradeComponent },
    { path: 'books',          component: BookComponent },
    { path: 'bookrequests',   component: BookrequestsComponent },
    { path: 'bookrequestshistory',   component: BookrequestshistoryComponent },
    { path: 'students',   component: StudentComponent }, 
    
];
