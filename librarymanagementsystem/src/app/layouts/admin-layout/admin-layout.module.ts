import { NgModule } from '@angular/core';
 ;
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DashboardComponent }       from '../../pages/dashboard/dashboard.component';
import { UserComponent }            from '../../pages/user/user.component';
import { TableComponent }           from '../../pages/table/table.component';
import { TypographyComponent }      from '../../pages/typography/typography.component';
import { IconsComponent }           from '../../pages/icons/icons.component';
import { MapsComponent }            from '../../pages/maps/maps.component';
import { NotificationsComponent }   from '../../pages/notifications/notifications.component';
import { UpgradeComponent }         from '../../pages/upgrade/upgrade.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
 
//import { AdminLayoutRoutes } from './admin-layout.routing';
//import { BooksRoutes } from 'app/pages/books/books.routing';
//import { DfComponent } from 'app/pages/books/df.component';


@NgModule({
  imports: [
    CommonModule,
    //RouterModule.forChild(AdminLayoutRoutes),
    //RouterModule.forRoot(BooksRoutes),
     
    FormsModule,
    NgbModule, 
  ],
  declarations: [
    DashboardComponent,
    UserComponent,
    TableComponent,
    UpgradeComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent, 
    //DfComponent
  ],
  providers: [ 
  ]
})

export class AdminLayoutModule {}
