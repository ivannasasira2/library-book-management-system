
import { Pipe, PipeTransform } from '@angular/core';

import { BookRequest } from '../classes/bookRequest';

@Pipe({
  name: 'searchBookRequest'
})

export class SearchBookRequestPipe implements PipeTransform {

 //tnsform(books_list: books[], searchValue: string): books[] 
  transform(objs_list: BookRequest[], searchValue: string): BookRequest[] {
   
    //if books_list is not defined or SearcValue not defined-----------------------
    if(!objs_list || !searchValue){
        return objs_list;
    }
   
    //If Both are defined--------------------
    return objs_list.filter(
      obj => obj.stud_name.toLocaleLowerCase()
      .includes(searchValue.toLocaleLowerCase())
     // || student.regNo.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
    );
    
  }

}
