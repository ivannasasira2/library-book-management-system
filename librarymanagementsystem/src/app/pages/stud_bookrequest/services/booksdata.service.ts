import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';

@Injectable({
   providedIn: 'root'
})

export class BooksdataService {

  constructor(private http: HttpClient) { }

  get_data(): Observable<any> {
      const url = "http://127.0.0.1:8000/api/book/to-request";
      return this.http.get<any>(url);
  }

  store_data(postData) {
    const url = "http://127.0.0.1:8000/api/book/request/";
     return this.http.post<any>(url, postData);
  }

  find_data(id){
    const url = "http://127.0.0.1:8000/api/books/"+id;
    return this.http.get<any>(url);
  }

  update_data(postData,id) {
     const url = "http://127.0.0.1:8000/api/book/update/"+id;
     return this.http.post<any>(url, postData);
  }

  destroy_data(id) {  
     const url = "http://127.0.0.1:8000/api/book/del/"+id; 
     return this.http.get<any>(url);
  }

}