import { DatePipe, getLocaleDateFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Book } from './classes/book';
import { BooksdataService } from './services/booksdata.service';

@Component({
    selector: 'book-cmp',
    moduleId: module.id,
    templateUrl: 'book.component.html'
})


export class BookComponent implements OnInit{
 
    searchValue:string; //searchable by book title
    searchBookValue: string;
    date: Date;
    return_date:string;

    books_list: Book[];
    book_title = "MITE"; 

    constructor(private objDataService: BooksdataService,
        private datePipe: DatePipe,
        ){ 
        //get data from the Api
        this.getDataFromApi()
    }

    ngOnInit(){ 
        
    }

    //get data from the Api
    getDataFromApi() {
        this.objDataService.get_data().subscribe((data)=>{
          //console.log(data)
          this.books_list = data
        }); 
    }
  

   // Delete Function-------------------------
   requestBook(id) {  
    //Comfirm Deletion------------------------------------
    var result = confirm("Are you sure you want to request for this book?");
    if (result) {
        //Logic to delete the item but do nothing 
    } else { return false; }

    //Return Date maths -----------------------
    this.date = new Date();
    this.date.setDate( this.date.getDate() + 10 );
    this.return_date= this.datePipe.transform(this.date,"yyyy-MM-dd");
        
   //Post Data--------------------------------
    let postData = {
            book_id:+id,
            student_id: 2,
            auto_return_date: this.return_date
    };


    //Call http delete service--------------------------------- 
     this.objDataService.store_data(postData).toPromise().then(
             (data)=>{
                   //console.log(data)
             });
    //reoad page
    alert('Book Request has been Submitted!');
    window.location.reload();
    }
    //End of the delete Function---
}
