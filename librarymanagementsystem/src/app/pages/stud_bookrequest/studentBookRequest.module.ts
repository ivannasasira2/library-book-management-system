import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SearchBookRequestPipe } from './services/searchBookRequest.pipe';
import { BookRequestDataService} from './services/bookRequestData.service';

import { BookRequestComponent} from './bookRequest.component'; 
import { BookComponent } from './book.component';
import { SearchbookPipe } from './services/searchbook.pipe';
 
@NgModule({
  imports: [ 
    CommonModule,
    FormsModule,  
    ReactiveFormsModule,
  ],
  declarations: [  
    BookRequestComponent,
    BookComponent,
    SearchBookRequestPipe,
    SearchbookPipe,
    
  ], 
  providers: [  
    BookRequestDataService,
    DatePipe
  ],
})
export class StudentBookRequestModule { }
