import { getLocaleDateFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { BookRequest } from './classes/bookRequest';
import { BookRequestDataService } from './services/bookRequestData.service';

@Component({
    selector: 'book-request-cmp',
    moduleId: module.id,
    templateUrl: 'bookRequest.component.html'
})


export class BookRequestComponent implements OnInit{
  
    search_value: string;

    records_list: BookRequest[];
    book_title = "MITE"; 

    constructor(private objDataService: BookRequestDataService){ 
        //get data from the Api
        this.getDataFromApi()
    }

    ngOnInit(){
   
    }

    //get data from the Api
    getDataFromApi() {
        this.objDataService.get_data().subscribe((data)=>{
          //console.log(data)
          this.records_list = data
        }); 
    }
  

   // Delete Function-------------------------
   deleteData_ToApi(id) {  
    //Comfirm Deletion------------------------------------
    var result = confirm("Want to delete?");
    if (result) {
        //Logic to delete the item but do nothing 
    } else { return false; }

    //Call http delete service--------------------------------- 
     this.objDataService.destroy_data(id).toPromise().then(
             (data)=>{
                   //console.log(data)
             });
    //reoad page
    alert('Record has been Deleted!');
    window.location.reload();
    }
    //End of the delete Function---
}
