import { TestBed } from '@angular/core/testing';

import { BookRequestDataService } from './bookRequestData.service';

describe('BookRequestDataService', () => {
  let service: BookRequestDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BookRequestDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
