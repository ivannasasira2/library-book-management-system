import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SearchBookRequestPipe } from './services/searchBookRequest.pipe';
import { BookRequestDataService} from './services/bookRequestData.service';

import { BookRequestComponent} from './bookRequest.component'; 
 import { ManageBookRequestComponent } from './manageBookRequest.component';

@NgModule({
  imports: [ 
    CommonModule,
    FormsModule,  
    ReactiveFormsModule,
  ],
  declarations: [  
    BookRequestComponent,
    SearchBookRequestPipe,
    ManageBookRequestComponent,
  ], 
  providers: [  
    BookRequestDataService
  ],
})
export class BookRequestModule { }
