export interface BookRequest { 
    id?:number;
    book_id:number;
    book_title?:string;
    student_id:number;
    librarian_id:number;
    return_date:string;
    auto_return_date?:string; 
    stud_name?:string,
    stud_reg_no?:string,
    stud_no?:number,
    lirarian_name?:string,
    created_at?:string,
    updated_at?:string,
}
