import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BookRequest } from './classes/bookRequest';
import { BookRequestDataService } from './services/bookRequestData.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'manageBookRequest-cmp',
   // moduleId: module.id,
    templateUrl: 'manageBookRequest.component.html'
})

export class ManageBookRequestComponent implements OnInit { 
    private selectedId: number; 

    //Student property --change
    bookRequest: BookRequest; 
 
    isAddMode: boolean;
    id:number; 
 
    constructor(private _route: ActivatedRoute, 
        private router:Router,
        private objDataService: BookRequestDataService ){
            //this forces a refresh of the page----
          //  this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        }


    ngOnInit(){
        //input id parameter
        this.selectedId = +this._route.snapshot.paramMap.get('id'); 
        this.get_obj(this.selectedId);  
    }


    get_obj(obj_id:number){ 
  
            this.bookRequest = { //--change
                id: null,
                book_title: null,
                book_id: null,
                student_id:null,
                librarian_id: null,
                return_date: null,
                stud_name: null,
                stud_reg_no: null,
                stud_no: null,
                lirarian_name: null,
            }; 
            
            if(obj_id===0){ //new form, create new book --initialize to null
                this.isAddMode =true;
            }else{ //get  book details from api-----

                    this.objDataService.find_data(obj_id).subscribe(  //--change
                        (api_Data)=>{ 
                            //  console.log(api_Data); 
                            let obj  = { //--change
                                id: obj_id,
                                book_title: api_Data.book_title,
                                book_id: 14,
                                student_id:  api_Data.student_id,
                                librarian_id: 1,
                                return_date: api_Data.auto_return_date,
                                stud_name: api_Data.stud_name,
                                stud_reg_no: api_Data.stud_reg_no,
                                stud_no: api_Data.stud_no,
                                lirarian_name: api_Data.lirarian_name,
                          };
                    //this.student_details=api_Data.stud_name;
                    this.bookRequest = Object.assign({},obj); // --change
                  });  
            }

    }

     

    issue_book(objForm: NgForm){
            
            let obj_id = this.selectedId;  
            
            //Define Parameters------------
           // let regNo = objForm.value.regNo;
  
            let post_objData = { 
                 id:obj_id, //book request id --comment
                 is_issued: 1,
                 librarian_id: 1,
            };

            //Store Method and Update Method
            if(obj_id>0){ 
                this.updateData_ToApi(post_objData,obj_id);
            }

            //navigate to a different route
    }

   reject_book(objForm: NgForm){
            
        let obj_id = this.selectedId;  

        //Comfirm Deletion------------------------------------
        var res= confirm("Want to Reject this Book Request?");
        if (res) {
            //Logic to delete the item but do nothing 
        } else { return false; }

        //Define Parameters------------
        //let regNo = objForm.value.regNo;
 
        let post_objData = { 
             id:obj_id, //book request id --comment
             is_issued: 2,
             librarian_id: 1
        };

        //Store Method and Update Method
        if(obj_id>0){ 
            this.updateData_ToApi(post_objData,obj_id);
        }

        //navigate to a different route
}
 

// call the update service
    updateData_ToApi(jsonPostData,id) {  
        this.objDataService.update_data(jsonPostData,id).toPromise().then(
            (data)=>{
                 // console.log(data)
                  alert('Book Request has been checked out!');
                  this.router.navigate(['/admin/book-requests']);
                });  
    }

}