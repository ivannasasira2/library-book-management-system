
import { Pipe, PipeTransform } from '@angular/core';

import { Book } from '../classes/book';

@Pipe({
  name: 'searchbook'
})

export class SearchbookPipe implements PipeTransform {

 //tnsform(books_list: books[], searchValue: string): books[] 
  transform(objs_list: Book[], searchValue: string): Book[] {
   
    //if books_list is not defined or SearcValue not defined-----------------------
    if(!objs_list || !searchValue){
        return objs_list;
    }
   
    //If Both are defined--------------------
    return objs_list.filter(
      obj => obj.title.toLocaleLowerCase()
      .includes(searchValue.toLocaleLowerCase())
     // || book.publicationDate.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
    );
    
  }

}
