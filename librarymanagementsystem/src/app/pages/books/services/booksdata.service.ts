import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';

@Injectable({
   providedIn: 'root'
})

export class BooksdataService {

  constructor(private http: HttpClient) { }

  get_data(): Observable<any> {
    //  const url = "http://localhost/sampledata/books.php";
      const url = "http://127.0.0.1:8000/api/books";
      return this.http.get<any>(url);
  }

  store_data(postData) {
    // let url = "http://localhost/sampledata/post_books.php";
     const url = "http://127.0.0.1:8000/api/book/create";
     return this.http.post<any>(url, postData);
  }

  find_data(id){ //show(id)
    const url = "http://127.0.0.1:8000/api/books/"+id;
    return this.http.get<any>(url);
  }

  update_data(postData,id) {
    // let url = "http://localhost/sampledata/post_books.php";
     const url = "http://127.0.0.1:8000/api/book/update/"+id;
     return this.http.post<any>(url, postData);
  }

  destroy_data(id) {  
     const url = "http://127.0.0.1:8000/api/book/del/"+id; 
     return this.http.get<any>(url);
  }

}