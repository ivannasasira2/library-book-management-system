import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SearchbookPipe } from './services/searchbook.pipe';
import { BooksdataService } from './services/booksdata.service';

import { BookComponent } from './book.component';
import { BookrequestsComponent } from './bookrequests.component';
import { BookrequestshistoryComponent } from './bookrequestshistory.component';
import { AddEditBookComponent } from './addEditBook.component';
// import { DfComponent } from './df.component';
// import { RouterModule } from '@angular/router';
 
//import { RouterModule } from '@angular/router'; 
//import { BooksRoutes } from './books.routing';

@NgModule({
  imports: [ 
    CommonModule,
    FormsModule, 
    //DfComponent,
    //RouterModule.forRoot(BooksRoutes),
    ReactiveFormsModule,
  ],
  declarations: [ 
    BookComponent,
    BookrequestsComponent,
    BookrequestshistoryComponent,
    SearchbookPipe,
    AddEditBookComponent,
  ], 
  providers: [ 
    BooksdataService
  ],
})
export class BooksModule { }
