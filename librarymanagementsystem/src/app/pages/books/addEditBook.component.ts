import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from './classes/book';
import { BooksdataService } from './services/booksdata.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'addeditbook-cmp',
   // moduleId: module.id,
    templateUrl: 'addEditBook.component.html'
})
export class AddEditBookComponent implements OnInit { 
    private selectedBookId: number; 
    //@Input() book=Book;

    // book: Book[];
    //book property
    book: Book; 
    isAddMode: boolean;
    id:number;

    //field parameters 
    //
    bookTitle:string;


    constructor(private _route: ActivatedRoute, 
        private router:Router,
        private objDataService: BooksdataService ){
            //this forces a refresh of the page----
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        }


    ngOnInit(){
        // /snapshot.paramMap.get('id');
        this.selectedBookId = +this._route.snapshot.paramMap.get('id');
        //alternative
        // this.selectedBookId = this._route.paramMap.subscribe(parameterBookId => {
        //    const id= +parameterBookId.get('id');
        //       return id;
        // });
        //alert('oops'+this.selectedBookId);
        
        
        this.getBook(this.selectedBookId);
        this.bookTitle = this.book.title;

    }


    getBook(bk_id:number){ 
  
            this.book = {
                id: null,
                title: null,
                publication_date:null,
                librarian_id: null
            };
         
            if(bk_id===0){ //new form, create new book --initialize to null
                this.isAddMode =true;
            }else{ //get  book details from api-----

                    this.objDataService.find_data(bk_id).subscribe( 
                        (api_BookData)=>{ 
                            // console.log(api_BookData) 
                            let bk  = {
                                id: bk_id,
                                title:  api_BookData.title,
                                publication_date: '2018-09-23',
                                librarian_id: 5
                          };
                          this.book = Object.assign({},bk); // bk;
                  }); 
            }

    }

     

    saveBook(bkForm: NgForm){
            //console.log(bkForm.value.bookTitle);
            //call different methods
            //Store Method and Update Method
            let b_id = this.selectedBookId;
            let book_title = bkForm.value.bookTitle;
            let post_bookData:Book;
            post_bookData = {"title":book_title,"publication_date":"2012-09-23"};

            if(b_id===0){ //create new book 
                this.postData_ToApi(post_bookData);
            }else{ //Update details-----
                this.updateData_ToApi(post_bookData,b_id);
            }

            //navigate to a different route
    }

//callthe post service
    postData_ToApi(jsonPostData) {  
        this.objDataService.store_data(jsonPostData).toPromise().then(
            (data)=>{
                 // console.log(data)
                  alert('Book has been Added!');
                  this.router.navigate(['/admin/books']);
                 // this.updateStatusMessage = "Book -"+this.bookTitle+" Added Successfully!";
            });  
    }

// call the update service
    updateData_ToApi(jsonPostData,id) {  
        this.objDataService.update_data(jsonPostData,id).toPromise().then(
            (data)=>{
                 // console.log(data)
                  alert('Book has been Updated!');
                  this.router.navigate(['/admin/books']);
                 // this.updateStatusMessage = "Book -"+this.bookTitle+" Added Successfully!";
            });  
    }

   
    // get_bookData(id){
    //    // const bk = this.booksData.find(id);
    //     this.booksData.find(id).subscribe((api_BookData)=>{
    //         //console.log(data) 
    //         this.book = {
    //             id: id,
    //             title: api_BookData.title,
    //             publication_date: '2018-09-23',
    //             librarian_id: 5
    //         };
    //       });

    // }

    editBook(){
        //trying to read id from the 
       // this.router.navigate(['/edit',this.bookId]);

    }

}