import { getLocaleDateFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Book } from './classes/book';
import { BooksdataService } from './services/booksdata.service';

@Component({
    selector: 'book-cmp',
    moduleId: module.id,
    templateUrl: 'book.component.html'
})


export class BookComponent implements OnInit{
 
    searchValue:string; //searchable by book title
    searchBookValue: string;

    books_list: Book[];
    book_title = "MITE"; 

    constructor(private objDataService: BooksdataService){ 
        //get data from the Api
        this.getDataFromApi()
    }

    ngOnInit(){
   
    }

    //get data from the Api
    getDataFromApi() {
        this.objDataService.get_data().subscribe((data)=>{
          //console.log(data)
          this.books_list = data
        }); 
    }
  

   // Delete Function-------------------------
   deleteData_ToApi(id) {  
    //Comfirm Deletion------------------------------------
    var result = confirm("Want to delete?");
    if (result) {
        //Logic to delete the item but do nothing 
    } else { return false; }

    //Call http delete service--------------------------------- 
     this.objDataService.destroy_data(id).toPromise().then(
             (data)=>{
                   //console.log(data)
             });
    //reoad page
    alert('Book has been Deleted!');
    window.location.reload();
    }
    //End of the delete Function---
}
