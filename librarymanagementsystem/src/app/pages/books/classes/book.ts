export interface Book {
    id?:number;
    title:string;
    publication_date:string;
    librarian_id?: number;
}