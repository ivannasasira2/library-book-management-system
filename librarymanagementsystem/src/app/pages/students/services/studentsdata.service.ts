import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';

@Injectable({
   providedIn: 'root'
})

export class StudentsdataService {

  constructor(private http: HttpClient) { }

  get_data(): Observable<any> {
    //  const url = "http://localhost/sampledata/books.php";
      const url = "http://127.0.0.1:8000/api/student";
      return this.http.get<any>(url);
  }

  store_data(postData) {
    // let url = "http://localhost/sampledata/post_books.php";
     const url = "http://127.0.0.1:8000/api/student";
     return this.http.post<any>(url, postData);
  }

  find_data(id){ //show(id)
    const url = "http://127.0.0.1:8000/api/student/"+id;
    return this.http.get<any>(url);
  }

  update_data(postData,id) { 
    // let url = "http://localhost/sampledata/post_books.php";
     const url = "http://127.0.0.1:8000/api/student/"+id;
     //put method --for laravenresource
     return this.http.put<any>(url, postData); 
  }

  destroy_data(id) {  
     const url = "http://127.0.0.1:8000/api/student/"+id; 
     return this.http.delete<any>(url);
  }

}