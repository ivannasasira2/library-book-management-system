
import { Pipe, PipeTransform } from '@angular/core';

import { Student } from '../classes/student';

@Pipe({
  name: 'searchStudent'
})

export class SearchstudentPipe implements PipeTransform {

 //tnsform(books_list: books[], searchValue: string): books[] 
  transform(students_list: Student[], searchValue: string): Student[] {
   
    //if books_list is not defined or SearcValue not defined-----------------------
    if(!students_list || !searchValue){
        return students_list;
    }
   
    //If Both are defined--------------------
    return students_list.filter(
      student => student.name.toLocaleLowerCase()
      .includes(searchValue.toLocaleLowerCase())
     // || student.regNo.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
    );
    
  }

}
