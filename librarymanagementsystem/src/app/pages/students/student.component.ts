import { getLocaleDateFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Student } from './classes/student';
import { StudentsdataService } from './services/studentsdata.service';

@Component({
    selector: 'student-cmp',
    moduleId: module.id,
    templateUrl: 'student.component.html'
})


export class StudentComponent implements OnInit{
  
    search_value: string;

    records_list: Student[];
    book_title = "MITE"; 

    constructor(private studentsData: StudentsdataService){ 
        //get data from the Api
        this.getDataFromApi()
    }

    ngOnInit(){
   
    }

    //get data from the Api
    getDataFromApi() {
        this.studentsData.get_data().subscribe((data)=>{
          //console.log(data)
          this.records_list = data
        }); 
    }
  

   // Delete Function-------------------------
   deleteData_ToApi(id) {  
    //Comfirm Deletion------------------------------------
    var result = confirm("Want to delete?");
    if (result) {
        //Logic to delete the item but do nothing 
    } else { return false; }

    //Call http delete service--------------------------------- 
     this.studentsData.destroy_data(id).toPromise().then(
             (data)=>{
                   //console.log(data)
             });
    //reoad page
    alert('Record has been Deleted!');
    window.location.reload();
    }
    //End of the delete Function---
}
