import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Student } from './classes/student';
import { StudentsdataService } from './services/studentsdata.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'addeditstudent-cmp',
   // moduleId: module.id,
    templateUrl: 'addEditStudent.component.html'
})

export class AddEditStudentComponent implements OnInit { 
    private selectedId: number; 

    //Student property --change
    student: Student; 
 
    isAddMode: boolean;
    id:number; 

    constructor(private _route: ActivatedRoute, 
        private router:Router,
        private studentsData: StudentsdataService ){
            //this forces a refresh of the page----
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        }


    ngOnInit(){
        //input id parameter
        this.selectedId = +this._route.snapshot.paramMap.get('id'); 
        this.get_obj(this.selectedId);  
    }


    get_obj(obj_id:number){ 
  
            this.student = { //--change
                id: null,
                regNo: null,
                studentNo:null,
                name: null
            }; 
            
            if(obj_id===0){ //new form, create new book --initialize to null
                this.isAddMode =true;
            }else{ //get  book details from api-----

                    this.studentsData.find_data(obj_id).subscribe(  //--change
                        (api_Data)=>{ 
                            // console.log(api_BookData) 
                            let obj  = { //--change
                                id: obj_id,
                                regNo: '289/272',
                                studentNo: 128373573,
                                name:  api_Data.name,
                                publication_date: '2018-09-23',
                                librarian_id: 5
                          };

                    this.student = Object.assign({},obj); // --change
                  });  
            }

    }

     

    create_update_Data(objForm: NgForm){
            
            let obj_id = this.selectedId;  
            
            //Define Parameters------------
            let name = objForm.value.name;
            let regNo = objForm.value.regNo;
            //let studentNo = objForm.value.studentNo; 

            let post_objData:Student;
            post_objData = {
                 "name":name, 
                 "regNo":"U/2389/2721",
                 "studentNo":192373573
            };

            //Store Method and Update Method
            if(obj_id===0){ //create new book 
                this.postData_ToApi(post_objData);
            }else{ //Update details----- 
                this.updateData_ToApi(post_objData,obj_id);
            }

            //navigate to a different route
    }

//callthe post service
    postData_ToApi(jsonPostData) {  
        this.studentsData.store_data(jsonPostData).toPromise().then(
            (data)=>{
                 // console.log(data)
                  alert('Student has been Added!');
                  this.router.navigate(['/admin/students']);
            });  
    }

// call the update service
    updateData_ToApi(jsonPostData,id) {  
        this.studentsData.update_data(jsonPostData,id).toPromise().then(
            (data)=>{
                 // console.log(data)
                  alert('Student has been Updated!');
                  this.router.navigate(['/admin/students']);
                });  
    }

}