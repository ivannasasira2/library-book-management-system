import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SearchstudentPipe } from './services/searchstudent.pipe';
import { StudentsdataService } from './services/studentsdata.service';

import { StudentComponent } from './student.component'; 
 import { AddEditStudentComponent } from './addEditStudent.component';

@NgModule({
  imports: [ 
    CommonModule,
    FormsModule,  
    ReactiveFormsModule,
  ],
  declarations: [ 
    StudentComponent, 
    SearchstudentPipe,
    AddEditStudentComponent,
  ], 
  providers: [ 
    StudentsdataService
  ],
})
export class StudentsModule { }
