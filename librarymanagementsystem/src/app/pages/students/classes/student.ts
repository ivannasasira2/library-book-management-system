export interface Student { 
    id?:number;
    regNo:string;
    studentNo:number;
    name:string;
    course_code?:string;
}
