import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UserLoginDataService} from './services/userLoginData.service';

import { LoginComponent } from './login.component';
import { LocalStorageService } from 'ngx-webstorage';

@NgModule({
  imports: [ 
    CommonModule,
    FormsModule,  
    ReactiveFormsModule,
  ],
  declarations: [   
    LoginComponent,
  ], 
  providers: [  
    UserLoginDataService,
    LocalStorageService,
  ],
})
export class LoginModule { }
