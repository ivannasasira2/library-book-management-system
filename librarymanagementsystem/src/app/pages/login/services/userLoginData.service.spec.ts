import { TestBed } from '@angular/core/testing';

import { UserLoginDataService } from './userLoginData.service';

describe('BookRequestDataService', () => {
  let service: UserLoginDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserLoginDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
