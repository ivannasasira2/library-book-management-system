import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
 
import { UserLoginDataService } from './services/userLoginData.service';
import { Observable } from 'rxjs';
import { LocalStorageService } from 'ngx-webstorage';
import { AuthorizeService } from 'app/authorize.service'; 

@Component({
    selector: 'login-cmp',
   // moduleId: module.id,
    templateUrl: 'login.component.html'
})
 
export class LoginComponent implements OnInit {  
    //Student property --change
    uname: string;
    pwd: string;   
 
    constructor(private _route: ActivatedRoute, 
        private router:Router, 
        private objDataService: UserLoginDataService,
        private localStorageService: LocalStorageService,
        private auth: AuthorizeService, 
        ){
            //this forces a refresh of the page----
          //  this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        }


    ngOnInit(){
        if(this._route.snapshot.paramMap.get('par')){
            let par= this._route.snapshot.paramMap.get('par');
            if(par==='logout'){
                alert('Logging Out Reached/......'+par);
            }
        }
    }



 //User Login--------------------------------------
   userLogin(objForm: NgForm){ 

    // this.auth.getAuthToken('stevewallaceug@gmail.com','pass123');
    // return false;

    //Define Parameters------------
    let username = objForm.value.uname;
    let password = objForm.value.pwd;

    let post_objData = {  
           usunameername: 'stevewalalce@gamil.com', //username,
           pwd: 'pass123', // password,
    }; 

    //Store Method and Update Method
    this.attempt_userLogin(post_objData);

    //navigate to a different route
}
 
// call the update service
attempt_userLogin(jsonPostData) {  

    this.objDataService.userlogin_post(jsonPostData).toPromise().then(
        (data)=>{
                console.log(data)

                let cdata =  JSON.stringify(data);
                var JSONObject = JSON.parse(cdata);
                var is_slogin = Number(JSONObject["slogin"]);
 
                //On succesful login
                if(is_slogin==1){
                    //store data in the local storage
                    this.localStorageService.store('user',data);
                    //route to home page----------- 
                    let userType = this.localStorageService.retrieve('user').user.usertype;
                    //let userType=1;
                    if(userType==1 || userType==2){ //Admin Dashboard
                       this.router.navigate(['/admin/dashboard']); 
                    }else if(userType==3){ //Student Portal
                       this.router.navigate(['/student/book-requests']); //--kinda interchanged----
                    }

                    
                   //return false;
                    //console.log(cdata);  
                }


                 //On Failed Login because of ----Incorrect Username and Password
                if(is_slogin==0){
                    //route to the Login page-----
                   this.router.navigate(['user/login']);
                    //console.log(cdata);
                } 

              ///alert('User Login Check!');
            //  this.router.navigate(['/admin/book-requests']);
            });  
}


logout(){
    this.localStorageService.clear('user');
//     //route to login page
    alert('logged out ......');
    this.router.navigate(['user/login']);
 }


issue_book(objForm: NgForm){
            
    let obj_id=1;
    
    //Define Parameters------------
   let username = objForm.value.uname;
   let password = objForm.value.pwd;

    let post_objData = {  
         username: username,
         pwd: password,
    };

    //Store Method and Update Method
    if(obj_id>0){ 
        this.updateData_ToApi(post_objData,obj_id);
    }

    //navigate to a different route
}


// call the update service
    updateData_ToApi(jsonPostData,id) {  
        this.objDataService.update_data(jsonPostData,id).toPromise().then(
            (data)=>{
                 // console.log(data)
                  alert('Book Request has been checked out!');
                  this.router.navigate(['/admin/book-requests']);
                });  
    }

}