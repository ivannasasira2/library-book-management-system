import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AdminLayoutModule } from './layouts/admin-layout/admin-layout.module';
 
import { BookComponent } from './pages/books/book.component';
import { BookComponent as portal_BookComponent } from './pages/stud_bookrequest/book.component';

import { BookrequestsComponent } from './pages/books/bookrequests.component';
import { BookrequestshistoryComponent } from './pages/books/bookrequestshistory.component';
import { DfComponent } from './pages/books/df.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { IconsComponent } from './pages/icons/icons.component';
import { MapsComponent } from './pages/maps/maps.component';
import { NotificationsComponent } from './pages/notifications/notifications.component';
import { StudentComponent } from './pages/students/student.component';
import { TableComponent } from './pages/table/table.component';
import { TypographyComponent } from './pages/typography/typography.component';
import { UpgradeComponent } from './pages/upgrade/upgrade.component';
import { UserComponent } from './pages/user/user.component';

import { AddEditBookComponent } from './pages/books/addEditBook.component';
import { AddEditStudentComponent } from './pages/students/addEditStudent.component';
import { BookRequestComponent } from './pages/bookrequest/bookRequest.component';
import { ManageBookRequestComponent } from './pages/bookrequest/manageBookRequest.component';
import { LoginComponent } from './pages/login/login.component';

import { AuthGuardService as AuthGuard} from './auth-guard.service';

//this is disregarded........................

const routes: Routes = [
 
  {
    path:'user',
    component: AdminLayoutComponent,
    children : [
      { path: 'login',component: LoginComponent},
      { path: 'login/:par',component: LoginComponent}
    ]
  },
  {
    path:'student', canActivate: [AuthGuard],
    component: AdminLayoutComponent,
    children : [
      { path: 'book-requests',  component: portal_BookComponent },
      { path: 'chd',  component: BookComponent },
    ]
  },
  {
    path:'admin', canActivate: [AuthGuard],  //canActivateChild: [AuthGuard],
    component: AdminLayoutComponent,
    children : [
      { path: 'chd', component: BookComponent },
      { path: 'dashboard',      component: DashboardComponent },
      { path: 'user',           component: UserComponent },
      { path: 'table',          component: TableComponent},
      { path: 'typography',     component:  TypographyComponent },
      { path: 'icons',          component: IconsComponent  },
      { path: 'maps',           component: MapsComponent },
      { path: 'notifications',  component: NotificationsComponent },
      { path: 'upgrade',        component: UpgradeComponent }, 
      { path: 'bookrequests',   component: BookrequestsComponent  },
      { path: 'bookrequestshistory',   component: BookrequestshistoryComponent },
      { path: 'students',   component: StudentComponent },
      { path: 'ae-student/:id',   component: AddEditStudentComponent},
      { path: 'books',          component: BookComponent },
      { path: 'ae-book/:id', component: AddEditBookComponent },
      { path: 'book-requests',          component: BookRequestComponent },
      { path: 'm-book-request/:id', component: ManageBookRequestComponent }
    ]
  },
];



@NgModule({
  //import RouterModule --routes and export the same
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})


export class AppRoutingModule { }
