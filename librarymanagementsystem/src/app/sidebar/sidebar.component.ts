import { Component, OnInit } from '@angular/core';


export interface RouteInfo {
    path: string;
    title: string; 
    class: string;
}

export const ROUTES: RouteInfo[] = [
 
    { path: '#',         title: '#Books Management',      class: 'text-orange' },
    { path: '/admin/books',         title: 'Books',      class: '' },
    { path: '/admin/ae-book/0',         title: 'Post New Book',        class: '' },
    { path: '#',         title: '#Book Requests',            class: '' },
    { path: '/admin/book-requests',         title: 'Book Requests',               class: '' },
    { path: '/admin/book-requests#',   title: 'Borrowed Books',          class: '' },
   
   ];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
}
