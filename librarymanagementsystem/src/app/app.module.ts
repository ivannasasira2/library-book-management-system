import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule } from "ngx-toastr";

import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';

import { AppComponent } from './app.component';
// import { AppRoutes } from './app.routing';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';

import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
 
import { CommonModule } from "@angular/common";
import { AppRoutingModule } from "./app-routing.module";
import { DfComponent } from "./pages/books/df.component";
import { BooksModule } from "./pages/books/books.module";
import { StudentsModule } from "./pages/students/students.module";
import { BookRequestModule } from "./pages/bookrequest/bookRequest.module";
import { StudentBookRequestModule } from "./pages/stud_bookrequest/studentBookRequest.module";
import { LoginModule } from "./pages/login/login.module";
import { NgxWebstorageModule } from 'ngx-webstorage';

 @NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    DfComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    AppRoutingModule,
    // RouterModule.forRoot(AppRoutes/*,{
    //     useHash: true,
    //     relativeLinkResolution: 'legacy'
    // }*/),
    SidebarModule,
    NavbarModule,
    ToastrModule.forRoot(),
    FooterModule,
    FixedPluginModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BooksModule,
    StudentsModule,
    BookRequestModule,
    StudentBookRequestModule,
    LoginModule,
    NgxWebstorageModule.forRoot(),
  ],
  exports: [
   
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
